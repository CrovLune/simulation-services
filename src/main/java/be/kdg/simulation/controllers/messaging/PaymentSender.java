package be.kdg.simulation.controllers.messaging;

import be.kdg.simulation.domain.Payment;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class PaymentSender {
    private final RabbitTemplate rabbitTemplate;

    public PaymentSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendPayment(Payment payment){
        rabbitTemplate.convertAndSend("payments", payment);
    }

}
