package be.kdg.simulation.controllers.messaging;

import be.kdg.simulation.domain.Payment;
import be.kdg.simulation.domain.TakeIn;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class TakeInSender {
    private final RabbitTemplate rabbitTemplate;

    public TakeInSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendTakeIn(TakeIn takeIn){
        rabbitTemplate.convertAndSend("takeins", takeIn);
    }
}


