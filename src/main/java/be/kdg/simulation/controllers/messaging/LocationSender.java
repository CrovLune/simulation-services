package be.kdg.simulation.controllers.messaging;

import be.kdg.simulation.domain.Location;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class LocationSender {
    private final RabbitTemplate rabbitTemplate;

    public LocationSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendLocation(Location location){
        rabbitTemplate.convertAndSend("locations", location);
    }
}
