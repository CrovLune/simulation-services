package be.kdg.simulation.controllers.messaging;

import be.kdg.simulation.controllers.dtos.DeliveryRequestDTO;
import be.kdg.simulation.domain.DeliveryRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class DeliveryRequestSender {
    //? Webclient Setup
   private String url = "http://localhost:9091";
   private WebClient webClient = WebClient.builder().baseUrl(url).build();

    //? localhost:9091/api/request
    public DeliveryRequest sendRequest(DeliveryRequestDTO request) {
        return webClient.post()
                .uri("/api/request")
                .body(Mono.just(request), DeliveryRequestDTO.class)
                .retrieve()
                .bodyToMono(DeliveryRequest.class)
                .block();
    }
}
