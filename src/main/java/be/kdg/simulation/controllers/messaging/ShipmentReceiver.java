package be.kdg.simulation.controllers.messaging;

import be.kdg.simulation.domain.Shipment;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
@Component
public class ShipmentReceiver {
    // TODO use logging
    // TODO put queue name in application.properties
    // TODO use an objectmapper or a MessageConverter that is auto-injected

    @RabbitListener(queues = "shipments")
    public void listen(Shipment message) {
        // do something with this message...

        System.out.println(message);
    }
}
