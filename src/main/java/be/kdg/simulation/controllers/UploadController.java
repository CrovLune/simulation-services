package be.kdg.simulation.controllers;

import be.kdg.simulation.services.DeliveryRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * Aleksey Zelenskiy
 * 10/21/2020.
 */
@Controller
public class UploadController {
    private DeliveryRequestService deliveryRequestService;

    @Autowired
    public UploadController(DeliveryRequestService deliveryRequestService) {
        this.deliveryRequestService = deliveryRequestService;
    }



    @GetMapping("/")
    public String index() {
        return "index";
    }

    @PostMapping("/uploadcsvfile")
    public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {
        // validate file
        if (file.isEmpty()) {
            model.addAttribute("message", "Please select a CSV file to upload.");
            model.addAttribute("status", false);
        } else {
           try{
               deliveryRequestService.handleCSV(file);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                model.addAttribute("message", "An error occurred while processing the CSV file: " + ex.getMessage());
                model.addAttribute("status", false);
            }

        }
        return "index";
    }
}
