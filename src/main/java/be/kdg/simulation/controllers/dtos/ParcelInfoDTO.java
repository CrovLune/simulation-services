package be.kdg.simulation.controllers.dtos;

import be.kdg.simulation.domain.NotHomeAction;
import be.kdg.simulation.domain.Priority;
import be.kdg.simulation.domain.Status;
import lombok.Getter;
import lombok.Setter;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
public class ParcelInfoDTO {
    @Getter
    @Setter
    private Status status;
    @Getter
    @Setter
    private Priority priority;
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private NotHomeAction action;

    public ParcelInfoDTO() {
    }

    public ParcelInfoDTO(Status status, Priority priority, String address, NotHomeAction action) {
        this.status = status;
        this.priority = priority;
        this.address = address;
        this.action = action;
    }

    @Override
    public String toString() {
        return "ParcelInfoDTO{" +
                "status=" + status +
                ", priority=" + priority +
                ", address='" + address + '\'' +
                ", action=" + action +
                '}';
    }
}
