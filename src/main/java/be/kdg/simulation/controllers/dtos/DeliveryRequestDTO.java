package be.kdg.simulation.controllers.dtos;

import be.kdg.simulation.domain.NotHomeAction;
import be.kdg.simulation.domain.Priority;
import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.Setter;

public class DeliveryRequestDTO {
    @CsvBindByName
    @Getter
    @Setter
    private long customerId;
    @Getter
    @Setter
    private int weight;
    @Getter
    @Setter
    private int size;
    @Getter
    @Setter
    private Priority priority;
    @CsvBindByName
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private NotHomeAction action;

    public DeliveryRequestDTO(long customerId, int weight, int size, Priority priority, String address, NotHomeAction action) {
        this.customerId = customerId;
        this.weight = weight;
        this.size = size;
        this.priority = priority;
        this.address = address;
        this.action = action;
    }

    public DeliveryRequestDTO(){

    }

}

