package be.kdg.simulation.controllers.dtos;

public class SettingsDto {
    private int paymentMillis;

    public int getPaymentMillis() {
        return paymentMillis;
    }

    public void setPaymentMillis(int paymentMillis) {
        this.paymentMillis = paymentMillis;
    }
}
