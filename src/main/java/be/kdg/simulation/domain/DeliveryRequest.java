package be.kdg.simulation.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/21/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "delivery_responses")
public class DeliveryRequest implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    private long parcelId;
    @Getter
    @Setter
    private long customerId;
    @Getter
    @Setter
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate validUntil;
    @Getter
    @Setter
    public DeliveryRequestStatus status;


    public DeliveryRequest() {
    }

    public DeliveryRequest(double price, long parcelId, LocalDate validUntil) {
        this.parcelId = parcelId;
        this.validUntil = validUntil;
        this.price = price;
        this.status = DeliveryRequestStatus.NOT_PAID;
    }

    @Override
    public String toString() {
        return "DeliveryRequestResponse{" +
                "price=" + price +
                ", parcelId=" + parcelId +
                ", validUntil=" + validUntil +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryRequest response = (DeliveryRequest) o;
        return Double.compare(response.price, price) == 0 &&
                parcelId == response.parcelId &&
                Objects.equals(validUntil, response.validUntil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, parcelId, validUntil);
    }
}

