package be.kdg.simulation.domain;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
public enum Status {
    NOT_DELIVERED,
    BEING_DELIVERED,
    DELIVERED
}
