package be.kdg.simulation.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    private int xCoord;
    @Getter
    @Setter
    private int yCoord;
    @Getter
    @Setter
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timeStamp;

    public Location(int xCoord, int yCoord, LocalDateTime timeStamp) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.timeStamp = timeStamp;
    }

    public Location() {
    }

    @Override
    public String toString() {
        return "Location{" +
                ", xCoord=" + xCoord +
                ", yCoord=" + yCoord +
                ", timeStamp=" + timeStamp +
                '}';
    }
}