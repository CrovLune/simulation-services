package be.kdg.simulation.domain;

public enum NotHomeAction {
    PICKUP_POINT(0),
    NEIGHBOUR(1);


    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    NotHomeAction(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static NotHomeAction fromString(String t) {
        for (NotHomeAction b : NotHomeAction.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }

}
