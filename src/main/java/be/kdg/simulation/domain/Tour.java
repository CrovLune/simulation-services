package be.kdg.simulation.domain;


import be.kdg.simulation.domain.vehicles.Size;
import be.kdg.simulation.domain.vehicles.Vehicle;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "tours")
public class Tour {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Vehicle vehicle;
    @Getter
    @Setter
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PostCode> postcodes;
    //? in m3 moet <= de vehicle capacity zijn
    @Setter
    @Getter
    private Size routeCapacity;
    @Setter
    @Getter
    private LocalDate date;

    public Tour() {
    }

    public Tour(Vehicle vehicle, List<PostCode> postcodes, Size routeCapacity, LocalDate date) {
        this.vehicle = vehicle;
        this.postcodes = postcodes;
        this.routeCapacity = routeCapacity;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour tour = (Tour) o;
        return Objects.equals(vehicle, tour.vehicle) &&
                Objects.equals(postcodes, tour.postcodes) &&
                routeCapacity == tour.routeCapacity &&
                Objects.equals(date, tour.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicle, postcodes, routeCapacity, date);
    }
}
