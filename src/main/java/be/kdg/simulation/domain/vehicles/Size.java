package be.kdg.simulation.domain.vehicles;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
public enum Size {
    SIX(6),
    EIGHT(8),
    ELEVEN(11),
    FIFTEEN(15);


    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    Size(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static Size fromString(String t) {
        for (Size b : Size.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
