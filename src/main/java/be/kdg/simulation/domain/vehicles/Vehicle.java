package be.kdg.simulation.domain.vehicles;

import be.kdg.simulation.domain.Location;
import be.kdg.simulation.domain.Parcel;
import be.kdg.simulation.domain.Tour;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "vehicles")
public class Vehicle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Location location;

    @Getter
    @Setter
    @OneToMany(mappedBy = "vehicle",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Parcel> parcels;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Tour tour;

    @Getter
    @Setter
    private Size capacity;

    public Vehicle() {
    }

    public Vehicle(Location location, List<Parcel> parcels,Size capacity) {
        this.location = location;
        this.parcels = parcels;
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return tour == vehicle.tour &&
                Objects.equals(location, vehicle.location) &&
                Objects.equals(parcels, vehicle.parcels) &&
                Objects.equals(capacity, vehicle.capacity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, parcels, capacity);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", location=" + location +
                ", parcels=" + parcels +
                ", capacity=" + capacity +
                '}';
    }

}
