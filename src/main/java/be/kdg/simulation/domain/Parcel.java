package be.kdg.simulation.domain;

import be.kdg.simulation.domain.vehicles.Vehicle;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "parcels")
public class Parcel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    private long customerId;
    @Getter
    @Setter
    private int size;
    @Getter
    @Setter
    private int weight;
    @Getter
    @Setter
    private Priority priority;
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private NotHomeAction action;
    @Getter
    @Setter
    private double price;
    @Getter
    @Setter
    private LocalDate validUntil;
    @Getter
    @Setter
    private Boolean delivered;
    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle_id", nullable = true)
    private Vehicle vehicle;

    public Parcel() {
    }

    public Parcel(
            long customerId,
            int size,
            int weight,
            Priority priority,
            String address,
            NotHomeAction action,
            double price,
            LocalDate validUntil,
            Boolean delivered
    ) {
        this.customerId = customerId;
        this.size = size;
        this.weight = weight;
        this.priority = priority;
        this.address = address;
        this.action = action;
        this.price = price;
        this.validUntil = validUntil;
        this.delivered = delivered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcel parcel = (Parcel) o;
        return customerId == parcel.customerId &&
                size == parcel.size &&
                weight == parcel.weight &&
                Double.compare(parcel.price, price) == 0 &&
                priority == parcel.priority &&
                Objects.equals(address, parcel.address) &&
                action == parcel.action &&
                Objects.equals(validUntil, parcel.validUntil) &&
                Objects.equals(delivered, parcel.delivered);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, size, weight, priority, address, action, price, validUntil, delivered);
    }

    @Override
    public String toString() {
        return "Parcel{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", size=" + size +
                ", weight=" + weight +
                ", priority=" + priority +
                ", address='" + address + '\'' +
                ", action=" + action +
                ", price=" + price +
                ", validUntil=" + validUntil +
                ", delivered=" + delivered +
                '}';
    }
}
