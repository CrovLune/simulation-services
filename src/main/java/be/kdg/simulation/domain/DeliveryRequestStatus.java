package be.kdg.simulation.domain;

/**
 * Aleksey Zelenskiy
 * 11/5/2020.
 */
public enum DeliveryRequestStatus {

    NOT_PAID(0),
    PAID(1),
    TAKEN_IN(2),
    IGNORED(3);


    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    DeliveryRequestStatus(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }


    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}
