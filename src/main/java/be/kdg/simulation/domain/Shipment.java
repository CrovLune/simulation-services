package be.kdg.simulation.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
public class Shipment {
    @Getter
    @Setter
    private long tourId;
    @Getter
    @Setter

    private List<Long> ParcelIds;
    @Getter
    @Setter
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startDateTime;

    public Shipment() {
    }

    public Shipment(long tourId, List<Long> parcelIds, LocalDateTime startDateTime) {
        this.tourId = tourId;
        ParcelIds = parcelIds;
        this.startDateTime = startDateTime;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "tourId=" + tourId +
                ", ParcelIds=" + ParcelIds +
                ", startDateTime=" + startDateTime +
                '}';
    }
}