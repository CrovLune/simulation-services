package be.kdg.simulation.persistence;

import be.kdg.simulation.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
}
