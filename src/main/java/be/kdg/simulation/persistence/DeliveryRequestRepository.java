package be.kdg.simulation.persistence;

import be.kdg.simulation.domain.DeliveryRequest;
import be.kdg.simulation.domain.DeliveryRequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeliveryRequestRepository extends JpaRepository<DeliveryRequest, Long> {
    List<DeliveryRequest> findByStatus(DeliveryRequestStatus status);
}
