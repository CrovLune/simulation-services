package be.kdg.simulation.persistence;

import be.kdg.simulation.domain.vehicles.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
}
