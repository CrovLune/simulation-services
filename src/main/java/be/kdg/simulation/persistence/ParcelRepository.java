package be.kdg.simulation.persistence;

import be.kdg.simulation.domain.Parcel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Repository
public interface ParcelRepository extends JpaRepository<Parcel,Long> {
}
