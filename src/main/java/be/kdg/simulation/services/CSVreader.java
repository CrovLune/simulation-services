package be.kdg.simulation.services;

import be.kdg.simulation.controllers.dtos.DeliveryRequestDTO;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
@Component
public class CSVreader {

    public List<DeliveryRequestDTO> makeDeliveryRequests(MultipartFile file) throws Exception {
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

            // create csv bean reader
            CsvToBean<DeliveryRequestDTO> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(DeliveryRequestDTO.class)
                    .withIgnoreLeadingWhiteSpace(false)
                    .build();

            // convert `CsvToBean` object to list of delivery requests
            List<DeliveryRequestDTO> basicDeliveryRequests = csvToBean.parse();
            return basicDeliveryRequests;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
