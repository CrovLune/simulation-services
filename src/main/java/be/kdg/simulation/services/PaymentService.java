package be.kdg.simulation.services;

import be.kdg.simulation.controllers.messaging.PaymentSender;
import be.kdg.simulation.domain.DeliveryRequest;
import be.kdg.simulation.domain.DeliveryRequestStatus;
import be.kdg.simulation.domain.Payment;
import be.kdg.simulation.domain.TakeIn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * Aleksey Zelenskiy
 * 10/24/2020.
 */
@Service
@Transactional
public class PaymentService {
    private PaymentSender paymentSender;
    private DeliveryRequestService deliveryRequestService;

    public PaymentService(PaymentSender paymentSender, DeliveryRequestService deliveryRequestService) {
        this.paymentSender = paymentSender;
        this.deliveryRequestService = deliveryRequestService;
    }

    @Scheduled(fixedDelay = (10000))
    public void scheduledSender() {
        var notPaidRequest = deliveryRequestService.readFirstByStatus(DeliveryRequestStatus.NOT_PAID);
        if(notPaidRequest==null) return;
        //chance to not get paid
        if(new Random().nextFloat() > 0.8){
            System.out.println("ignoring parcel id: " + notPaidRequest.getParcelId());
            notPaidRequest.setStatus(DeliveryRequestStatus.PAID);
        }
        else {
            var payment = this.createPayment(notPaidRequest);
            System.out.println("sending payments for id: " + notPaidRequest.getParcelId());
            this.pay(payment);
            notPaidRequest.setStatus(DeliveryRequestStatus.PAID);
        }
        deliveryRequestService.save(notPaidRequest);
    }

    public Payment createPayment(DeliveryRequest deliveryRequestResponse) {
        var customerId = deliveryRequestResponse.getCustomerId();
        var parcelId = deliveryRequestResponse.getParcelId();
        var amount = deliveryRequestResponse.getPrice();
        return new Payment(customerId, parcelId, amount);
    }

    public void pay(Payment payment) {
        paymentSender.sendPayment(payment);
    }

}
