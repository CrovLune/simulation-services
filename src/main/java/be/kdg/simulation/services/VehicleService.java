package be.kdg.simulation.services;

import be.kdg.simulation.domain.Parcel;
import be.kdg.simulation.domain.Tour;
import be.kdg.simulation.domain.vehicles.Vehicle;
import be.kdg.simulation.persistence.VehicleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Service
@Transactional
public class VehicleService {
    Comparator<Vehicle> minComparator = (o1, o2) -> {
        var a = o1.getParcels().stream().map(Parcel::getSize).count() < o2.getParcels().stream().map(Parcel::getSize).count();
        if (a)
            return 1;
        else
            return 0;
    };
    private final VehicleRepository repo;

    public VehicleService(VehicleRepository repo) {
        this.repo = repo;
    }


    public long createVehicle(Vehicle vehicle) {
        var v = repo.save(vehicle);
        return v.getId();
    }


    public Vehicle readVehicleById(long id) {
        if (repo.findById(id).isPresent())
            return repo.findById(id).get();
        else
            return null;
    }


    public List<Vehicle> readAllVehicles() {
        if (!repo.findAll().isEmpty())
            return repo.findAll();
        else
            return null;
    }


    public void updateVehicle(Vehicle vehicle) {
        var t = readVehicleById(vehicle.getId());
        if (t != null)
            repo.save(vehicle);
        else
            //TODO: proper logging service.
            System.out.println("VehicleService: Vehicle not UPDATED, Probably Vehicle not found in DB");
    }


    public void updateVehicles(List<Vehicle> vehicles) {
        var t = readAllVehicles();
        if (t != null)
            repo.saveAll(vehicles);
        else
            //TODO: proper logging service.
            System.out.println("VehicleService: Vehicles not UPDATED, Probably Vehicle not found in DB");
    }


    public void deleteVehicle(Vehicle vehicle) {
        var t = readVehicleById(vehicle.getId());
        if (t != null)
            repo.delete(vehicle);
        else
            //TODO: proper logging service.
            System.out.println("VehicleService: Vehicle not DELETED, Probably Vehicle not found in DB");
    }


    public void deleteVehicles(List<Vehicle> vehicles) {
        var t = readAllVehicles();
        if (t != null)
            repo.deleteAll(vehicles);
        else
            //TODO: proper logging service.
            System.out.println("VehicleService: Vehicles not DELETED, Probably Vehicle not found in DB");
    }

    //? Compare the capacity of the vehicle with the capacity of the parcel.
    public List<Vehicle> compareToParcel(Parcel parcel) {
        return this.readAllVehicles().stream().filter(v -> v.getCapacity().getNumeric() >= parcel.getSize()).collect(Collectors.toList());
    }

    //? Compare the capacity of the vehicle with the capacity of the tour.
    public List<Vehicle> compareToTour(List<Vehicle> list, Tour t) {
        return list.stream().filter(v -> v.getCapacity().getNumeric() >= t.getRouteCapacity().getNumeric()).collect(Collectors.toList());
    }

    public Vehicle getLeastLoadedVehicle(List<Vehicle> list) {
        if (list.stream().min(minComparator).isPresent()) {
            return list.stream().min(minComparator).get();
        }
        //TODO: errorHandling
        return null;
    }

    public Vehicle getBestSuitedVehicle(Parcel parcel, Tour tour){
        var firstSort = this.compareToParcel(parcel);
        var finalSort = this.compareToTour(firstSort,tour);
        var bestVehicle = this. getLeastLoadedVehicle(finalSort);
        return bestVehicle;
    }
}
