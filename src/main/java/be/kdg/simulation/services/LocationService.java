package be.kdg.simulation.services;

import be.kdg.simulation.controllers.messaging.LocationSender;
import be.kdg.simulation.domain.Location;
import be.kdg.simulation.persistence.LocationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 10/26/2020.
 */
@Service
@Transactional
public class LocationService {
    private final LocationSender locationSender;
    private final LocationRepository repo;

    public LocationService(LocationSender locationSender, LocationRepository repo) {
        this.locationSender = locationSender;
        this.repo = repo;
    }


    public long create(Location location) {
        var l = repo.save(location);
        return l.getId();
    }


    public Location readById(long id) {
        if (repo.findById(id).isPresent())
            return repo.findById(id).get();
        else
            return null;
    }


    public List<Location> readAll() {
        if (!repo.findAll().isEmpty())
            return repo.findAll();
        else
            return null;
    }


    public void update(Location location) {
        var t = readById(location.getId());
        if (t != null)
            repo.save(location);
        else
            //TODO: proper logging service.
            System.out.println("LocationService: Location not UPDATED, Probably location not found in DB");
    }


    public void updateMultiple(List<Location> locations) {
        var t = readAll();
        if (t != null)
            repo.saveAll(locations);
        else
            //TODO: proper logging service.
            System.out.println("LocationService: Locations not UPDATED, Probably locations not found in DB");
    }


    public void delete(Location location) {
        var t = readById(location.getId());
        if (t != null)
            repo.delete(location);
        else
            //TODO: proper logging service.
            System.out.println("LocationService: Location not DELETED, Probably location not found in DB");
    }


    public void deleteMultiple(List<Location> locations) {
        var t = readAll();
        if (t != null)
            repo.deleteAll(locations);
        else
            //TODO: proper logging service.
            System.out.println("LocationService: Locations not DELETED, Probably locations not found in DB");
    }

    public void sendLocation(Location location) {
        locationSender.sendLocation(location);
    }
}
