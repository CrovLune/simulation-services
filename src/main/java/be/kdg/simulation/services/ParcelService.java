package be.kdg.simulation.services;

import be.kdg.simulation.domain.Parcel;
import be.kdg.simulation.persistence.ParcelRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Aleksey Zelenskiy
 * 10/25/2020.
 */
@Service
@Transactional
public class ParcelService {
    private final ParcelRepository repo;

    public ParcelService(ParcelRepository repo) {
        this.repo = repo;
    }

    public long createParcel(Parcel parcel) {
        var p = repo.save(parcel);
        return p.getId();
    }

    public Parcel readParcelById(long id) {
        if (repo.findById(id).isPresent())
            return repo.findById(id).get();
        else
            return null;
    }


    public List<Parcel> readAllParcels() {
        if (!repo.findAll().isEmpty())
            return repo.findAll();
        else
            return null;
    }


    public void updateParcel(Parcel parcel) {
        var t = readParcelById(parcel.getId());
        if (t != null)
            repo.save(parcel);
        else
            //TODO: proper logging service.
            System.out.println("ParcelService: Parcel not UPDATED, Probably parcel not found in DB");
    }


    public void updateParcels(List<Parcel> parcels) {
        var t = readAllParcels();
        if (t!=null)
            repo.saveAll(parcels);
        else
            //TODO: proper logging service.
            System.out.println("ParcelService: Parcel not UPDATED, Probably parcels not found in DB");
    }


    public void deleteParcel(Parcel parcel) {
        var t = readParcelById(parcel.getId());
        if (t != null)
            repo.delete(parcel);
        else
            //TODO: proper logging service.
            System.out.println("ParcelService: Parcel not DELETED, Probably parcel not found in DB");
    }


    public void deleteParcels(List<Parcel> parcels) {
        var t = readAllParcels();
        if (t!=null)
            repo.deleteAll(parcels);
        else
            //TODO: proper logging service.
            System.out.println("ParcelService: Parcels not DELETED, Probably parcels not found in DB");
    }

    public int getPostalCode(Parcel parcel){
        var postcodeString = parcel.getAddress().substring(parcel.getAddress().length() - 4);
        var postcode = Integer.parseInt(postcodeString);
        return postcode;
    }
}
