package be.kdg.simulation.services;

import be.kdg.simulation.controllers.messaging.DeliveryRequestSender;
import be.kdg.simulation.controllers.dtos.DeliveryRequestDTO;
import be.kdg.simulation.domain.DeliveryRequest;
import be.kdg.simulation.domain.NotHomeAction;
import be.kdg.simulation.domain.Priority;
import be.kdg.simulation.domain.DeliveryRequestStatus;
import be.kdg.simulation.persistence.DeliveryRequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class DeliveryRequestService {
    private final DeliveryRequestRepository deliveryRequestRepository;
    private CSVreader csvReader;
    private DeliveryRequestSender deliveryRequestSender;
    private Random random;


    public DeliveryRequestService(DeliveryRequestRepository deliveryRequestRepository, CSVreader csvReader, DeliveryRequestSender deliveryRequestSender) {
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.csvReader = csvReader;
        this.deliveryRequestSender = deliveryRequestSender;
        this.random = new Random();
    }

    public void handleCSV(MultipartFile file)throws Exception{

        var basicDeliveryRequests = csvReader.makeDeliveryRequests(file);


        //? Complete delivery request
        for (var d : basicDeliveryRequests) {
            var filledDTO= this.fillDTO(d);
            //? Send Request to Operational
            var response = deliveryRequestSender.sendRequest(filledDTO);
            response.setCustomerId(d.getCustomerId());
            response.setStatus(DeliveryRequestStatus.NOT_PAID);
            this.save(response);
        }
    }

    public DeliveryRequestDTO fillDTO(DeliveryRequestDTO dr) {

        var r = random.nextInt(10) + 1;
        if (r > 5) {
            dr.setAction(NotHomeAction.PICKUP_POINT);
        } else {
            dr.setAction(NotHomeAction.NEIGHBOUR);
        }

        if (r < 5) {
            dr.setPriority(Priority.SAMEDAY);
        } else {
            dr.setPriority(Priority.NEXTDAY);
        }
        dr.setSize(random.nextInt(10) + 1);
        dr.setWeight(random.nextInt(30) + 1);

        return dr;
    }

    public List<DeliveryRequest> readAllResponses(){
        return deliveryRequestRepository.findAll();
    }

    public DeliveryRequest readFirstByStatus(DeliveryRequestStatus status){
        try {
            return this.readAllByStatus(status).get(0);
        }catch (IndexOutOfBoundsException ex){
            return null;
        }
    };

    public List<DeliveryRequest> readAllByStatus(DeliveryRequestStatus status){
        return deliveryRequestRepository.findByStatus(status);
    };

    public void save(DeliveryRequest request){
        deliveryRequestRepository.save(request);
    }

}
