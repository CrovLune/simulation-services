package be.kdg.simulation.services;

import be.kdg.simulation.controllers.messaging.TakeInSender;
import be.kdg.simulation.domain.DeliveryRequest;
import be.kdg.simulation.domain.DeliveryRequestStatus;
import be.kdg.simulation.domain.TakeIn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TakeInService {

    private TakeInSender takeInSender;
    private DeliveryRequestService deliveryRequestService;

    public TakeInService(TakeInSender takeInSender, DeliveryRequestService deliveryRequestService) {
        this.takeInSender = takeInSender;
        this.deliveryRequestService = deliveryRequestService;
    }

    @Scheduled(fixedDelay = (30000))
    public void scheduledSender() {
        var paidRequest = deliveryRequestService.readFirstByStatus(DeliveryRequestStatus.PAID);
        if (paidRequest == null) return;

        var takeIn = this.createTakeIn(paidRequest);
        System.out.println("sending takein for id: "+ paidRequest.getParcelId());
        this.takeIn(takeIn);
        paidRequest.setStatus(DeliveryRequestStatus.TAKEN_IN);
        deliveryRequestService.save(paidRequest);
    }

    public TakeIn createTakeIn(DeliveryRequest deliveryRequest) {
        var parcelId = deliveryRequest.getParcelId();
        return new TakeIn(parcelId, LocalDateTime.now());
    }

    public void takeIn(TakeIn takeIn) {
        takeInSender.sendTakeIn(takeIn);
    }

}

