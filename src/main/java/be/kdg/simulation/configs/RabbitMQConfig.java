package be.kdg.simulation.configs;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    // TODO put queue name in application.properties
    @Bean
    public Queue paymentQueue() {
        return new Queue("payments", false);
    }
    @Bean
    public Queue takeInQueue() {
        return new Queue("takeins", false);
    }
}
