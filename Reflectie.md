
# Reflectie

Reflectie file voor SA project


## Status


+ DeliveryRequest POST, Payment receiver, en TakeInreceiver volledig uitgewerkt

+ Uitbreidbare PriceCalculation methode, met een beslisserklasse

+ Uitbreidbare TakeInChecks, kunnen toegevoegd worden zonder code te veranderen (met een list geïnjecteerd)

+ Error Handling worden gethrowed tot de controller of MessageReceiver die de service heeft aangeroepen

+ minimale code in controllers/MessageReceivers, alleen service aanroepen en error handling


